set -e
#  constant part!
RED='\033[1;31m'
BLUE='\033[1;34m'
GREEN='\033[1;32m'
NC='\033[0m' # No Color
DEST="pi@192.168.0.107"
#DEST="pi@192.168.43.161"
PROJECT_PATH=$DEST":/home/pi/robot"

printf "${BLUE}Deploying to ${PROJECT_PATH}...${NC}\n" &&

printf "${BLUE}Building project...${NC}\n" &&
gulp build &&

printf "${BLUE}Removing old files...${NC}\n" &&
sshpass -p "raspberry" ssh $DEST rm -rf "/home/pi/robot/dist" &&


printf "${BLUE}Copying new files ...${NC}\n" &&
sshpass -p "raspberry" scp -r ./dist $PROJECT_PATH &&
sshpass -p "raspberry" scp -r ./src $PROJECT_PATH &&
sshpass -p "raspberry" scp ./build.sh $PROJECT_PATH &&
sshpass -p "raspberry" scp ./start.sh $PROJECT_PATH &&
sshpass -p "raspberry" scp ./tsconfig.json $PROJECT_PATH &&
sshpass -p "raspberry" scp ./package.json $PROJECT_PATH &&
sshpass -p "raspberry" scp ./gulpfile.js $PROJECT_PATH &&

#printf "${BLUE}Restarting SecuBot service ...${NC}\n" &&
sshpass -p "raspberry" sudo systemctl restart secubot &&

printf  "${GREEN}Deploying done!${NC}\n" || printf  "${RED}Deploying errors!${NC}\n"
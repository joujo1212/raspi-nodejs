import {Server} from 'http';
import * as express from 'express';
import * as socketIo from 'socket.io-client';

import {Message} from '../model/message';
import {Event} from "../model/event.enum";
import {Action} from "../model/action.enum";
import {HW} from "../hw/hw";
import moment = require("moment");
// const hw = require('./hw/hw');
const wpi = require('node-wiring-pi');
// import { init } from 'raspi';
// import { SoftPWM } from 'raspi-soft-pwm';
// const SERVER_URL = 'http://localhost:8090';
// const SERVER_URL = 'http://192.168.43.107:8090';
const SERVER_URL = process.argv[2];
const ROBOT_ID = 'robot1';
// const raspi = require('raspi');
// const I2C = require('raspi-i2c').I2C;
var fs = require("fs");
var fileName = "test.txt";

export class ChatServer {
  public static readonly PORT: number = 8090;
  private app: express.Application;
  private port: string | number;
  private socket;

  constructor() {
    this.createApp();
    this.config();
    this.initSocket();
    this.onMessage((data: Message) => {
      console.log('Received: ', data);
    });
    if (wpi) {
      wpi.setup('gpio');
    }
  }

  private createApp(): void {
    this.app = express();
  }

  public initSocket(): void {
    console.log(`Trying to connect to server (${SERVER_URL})...`);

    // setInterval(() => {
    //
    //
    //   raspi.init(() => {
    //     const i2c = new I2C();
    //     console.log(i2c.readByteSync(0x04) * 20); // Read one byte from the device at address 18
    //     i2c.writeByteSync(0x04, undefined, 1); // Read one byte from the device at address 18
    //   });
    // }, 5000);


    // wpi.wiringPiI2CSetup(0x04);
    // setInterval(() => {
    //   let daco;


      // fs.exists(fileName, function(exists) {
      //   if (exists) {
          // get information about the file
          // fs.stat(fileName, function(error, stats) {
            // open the file (getting a file descriptor to it)
            // fs.open(fileName, "a", function(error, fd) {
            //   console.log('pisem...');
            //   // var buffer = new Buffer(stats.size);
            //   wpi.wiringPiI2CRead(fd);

              // read its contents into buffer
              // fs.read(fd, buffer, 0, buffer.length, null, function(error, bytesRead, buffer) {
              //   var data = buffer.toString("utf8", 0, buffer.length);
              //
              //   console.log(data);
              //   fs.close(fd);
              // });
            // });
          // });
        // }
      // });


    // }, 1000);


    this.socket = socketIo(SERVER_URL);
    this.socket.on('connect', () => {
      console.log('Robot connected to server');
      this.socket.on();
      this.onRobotChannel();
      this.socket.emit('robot/' + ROBOT_ID, {action: Action.ROBOT_STARTED});
    });
    this.socket.on('disconnect', () => {
      console.log('Robot disconnected from server (network/server is down)');
      this.socket.off('robot/' + ROBOT_ID);
    })
  }

  public send(message: Message): void {
    this.socket.emit('message', message);
  }

  public onMessage(callback) {
    this.socket.on('message', (data: Message) => callback(data));
  }

  public onEvent(event: Event, callback) {
    this.socket.on(event, callback);
  }

  public onRobotChannel() {
    this.socket.on('robot/' + ROBOT_ID, (message: Message) => {
      switch (message.action) {
        case Action.PING:
          console.log('ping -> pong');
          this.socket.emit('robot/' + ROBOT_ID, {action: Action.PONG});
          break;
        case Action.GET_BATTERY:
          console.log('Get battery level');
          this.socket.emit('robot/' + ROBOT_ID, {action: Action.BATTERY, data: {batteryLevel: this.getBatteryLevel()}});
          break;
        case Action.MOVE:
          console.log('Move to:', message.data);
          HW.move(message.data.left, message.data.right, message.data.dir);
          break;
        case Action.SEND_COMMAND:
          console.log('Command:', message.data.command);
          HW.executeCommand(message.data.command)
            .then(response => {
              console.log(response);
              this.socket.emit('robot/' + ROBOT_ID, {action: Action.COMMAND_RESPONSE, data: {response}});
            });
          break;
        case Action.TAKE_PHOTO:
          console.log('Command:', message.data.command);
          HW.executeCommand(message.data.command)
            .then(response => {
              // console.log(response);
              setTimeout(() => {
                const date = new Date();
                this.socket.emit('robot/' + ROBOT_ID, {
                  action: Action.TAKE_PHOTO_RESPONSE,
                  data: {
                    imageBase64: HW.base64_encode('/home/pi/robot/image.jpg'),
                    imageName: `photo-${moment().format('YYYY-M-D HH:mm:ss.SSS')}.jpg`
                  }
                });
              }, 1000);
            });
          break;
        case Action.TURN_CAMERA_ON:
          HW.executeCommand('sudo systemctl start secubot_cam')
            .then(response => {
              this.socket.emit('robot/' + ROBOT_ID, {action: Action.TURN_CAMERA_ON_RESPONSE});
            });
          break;
        case Action.TURN_CAMERA_OFF:
          HW.executeCommand('sudo systemctl stop secubot_cam')
            .then(response => {
              this.socket.emit('robot/' + ROBOT_ID, {action: Action.TURN_CAMERA_OFF_RESPONSE});
            });
          break;
        case Action.GET_BASIC_INFO:
          Promise.all([
            HW.executeCommand('ip route get 8.8.8.8 | awk \'{print $NF; exit}\''),
            HW.executeCommand('wget -qO- http://ipecho.net/plain ; echo'),
          ])
            .then(([localIp, publicIp]) => {
              const data = {
                localIp,
                publicIp
              };
              this.socket.emit('robot/' + ROBOT_ID, {action: Action.BASIC_INFO, data});
            })
          break;
      }
    });
  }

  private getBatteryLevel(): number {
    return Math.round(Math.random() * 100);
  }

  private config(): void {
    this.port = process.env.PORT || ChatServer.PORT;
  }

  public getApp(): express.Application {
    return this.app;
  }


}
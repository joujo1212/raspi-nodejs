// 'use strict';
// const log = require('../logger/logger');
// const cli = require('../cli/cli');
// const wpi = require('wiring-pi');
// const wpi = {
//     setup: function() {},
//     pinMode: function() {},
//     pullUpDnControl: function() {},
//     wiringPiISR: function() {},
//     digitalWrite: function() {},
//     move: function() {}
// };
// const TAG = 'HW';
//
// if (wpi) {
//     wpi.setup('gpio');
// }
//
// const registerButton = (gpioPin, callback) => {
//     var canButton = true;
//
//     wpi.pinMode(gpioPin, wpi.INPUT);
//     wpi.pullUpDnControl(gpioPin, wpi.PUD_UP);
//     wpi.wiringPiISR(gpioPin, wpi.INT_EDGE_FALLING, function() {
//         if (canButton === true) {
//             callback();
//             canButton = false;
//             setTimeout(() => canButton = true, 1000);
//         }
//     });
// };
//
// const beep = (millis) => {
//     const pin = 15;
//     wpi.pinMode(pin, wpi.OUTPUT);
//     wpi.digitalWrite(pin, 1);
//     setTimeout(() => wpi.digitalWrite(pin, 0), millis);
// };
//
// const blink = () => {
//     const pin= 21;
//     wpi.pinMode(pin, wpi.OUTPUT);
//     wpi.digitalWrite(pin, 1);
//     setTimeout(() => wpi.digitalWrite(pin, 0), 1000);
// };
//
// const initSleepButton = () => {
//     log.i(TAG, 'Sleep button initialized');
//     registerButton(3, () => {
//         log.i(TAG, 'Sleep button pressed');
//         cli.device.shutdown();
//     });
// };
//
// /**
//  *
//  * @param code number consists from 2 digits, digit 0 is not allowed
//  */
// const beepCode = (code) => {
//     let firstDigit = Math.floor(code / 10);
//     let secondDigit = code % 10;
//     const pause = 500;
//
//     for (let i = 0; i < firstDigit; i++) {
//         setTimeout(() => beep(200), i * pause);
//     }
//
//     for (let i = 1; i <= secondDigit; i++) {
//         setTimeout(() => beep(200), (firstDigit * pause) + (i * pause));
//     }
// };
//
// // TODO finish, its just test
// const move = (left, right, dir) => {
//   const out1 = 13;
//   const out2 = 11;
//   const out3 = 15;
//   const out4 = 12;
//   wpi.pinMode(out1, wpi.OUTPUT);
//   wpi.pinMode(out2, wpi.OUTPUT);
//   wpi.pinMode(out3, wpi.OUTPUT);
//   wpi.pinMode(out4, wpi.OUTPUT);
//   wpi.digitalWrite(out1, 0);
//   wpi.digitalWrite(out2, 0);
//   wpi.digitalWrite(out3, 0);
//   wpi.digitalWrite(out4, 0);
//   wpi.digitalWrite(out2, 1);
// };
//
// module.exports = {
//     button :{
//         initSleepButton: () => initSleepButton(),
//     },
//     beeper: {
//         beepOnce: () => beep(),
//         beepCode: code => beepCode(code)
//     },
//     blink: {
//         once: () => blink(),
//     },
//     move: move
// };

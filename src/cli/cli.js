const spawn = require('child_process').spawn;
const exec = require('child_process').exec;
const log = require('../logger/logger');
const hw = require('../hw/hwOLD');
const TAG = 'Cli';

const mjpgStreamerCommand = '/home/pi/server/video_base.sh';
const mjpgStreamerProcName = 'mjpg_streamer';

const execute = (cmd, args) => {
    exec(cmd, function(error, stdout, stderr) {
        log.i(TAG, `Executing command ${cmd}`);
    });
};

const getPid = (processName, onPid) => {
    exec('pidof ' + processName, function(error, stdout, stderr) {
        log.i(TAG, `Found PID ${stdout} of process ${processName}`);
        onPid(stdout);
    });
};

const killCamera = (cameraProcessPid) => {
    execute('kill -9 ' +  cameraProcessPid);
};

const startCamera = () => {
    spawn(mjpgStreamerCommand, []);
};

const shutdownDevice = () => {
    log.i(TAG, 'Shutting down device...');
    hw.beeper.beepCode(11);
    execute('sudo shutdown now');
};

module.exports = {
    stream: {
        start: () => startCamera(),
        stop: () => getPid(mjpgStreamerProcName, pid => killCamera(pid)),
    },
    device: {
        shutdown: () => shutdownDevice(),
    }
};

set -e
#  constant part!
RED='\033[1;31m'
BLUE='\033[1;34m'
GREEN='\033[1;32m'
NC='\033[0m' # No Color

printf  "${BLUE}Building project to ./dist ...${NC}\n"
gulp build
printf "${GREEN}Building done!${NC}\n"

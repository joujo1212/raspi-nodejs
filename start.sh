#!/bin/bash

# Change server url if needed
SERVER="http://159.65.205.222:8090"
#SERVER="http://192.168.0.102:8090"

#  constant part!
RED='\033[1;31m'
BLUE='\033[1;34m'
GREEN='\033[1;32m'
NC='\033[0m' # No Color

set -e
printf "${BLUE}Starting Robot app with server URL ${SERVER} ...${NC}\n"
node /home/pi/robot/dist/index.js $SERVER
